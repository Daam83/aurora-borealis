package com.daamapp.aurora;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by adam12 on 03.09.17.
 */

public class LoadHemisfere {

    private String inDate;
    /**
     * @param inDate - input value of string with give information wich site of ovation was chosen
     */

    private String toText;
    /**
     * @return toText - output value string are show in ovationSite.textview2
     */
    private String toULR;

    /**
     * @return toULR -  output value string wich contain URL to chosen ovation site.
     */


    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    /**
     * @return toText - if on inDate is north valuse then getter return string for northen ovation
     * if not getter return Southen Ovation
     */
    public String getToText() {
        if (inDate == "north") {
            toText = "Northen Ovation";
            return toText;
        } else {
            toText = "Southen Ovation";
            return toText;
        }


    }

    /**
     * geter getToULR() retun correct url to newest ovation  image.
     *
     * @return gete
     */

    public String getToULR() {

        toULR = "http://services.swpc.noaa.gov/images/animations/ovation-" + inDate + "/latest.jpg";
        return toULR;


    }
}
