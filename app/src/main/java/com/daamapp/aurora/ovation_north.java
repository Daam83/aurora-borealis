package com.daamapp.aurora;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;

public class ovation_north extends AppCompatActivity {
    private Activity myActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ovation_north);
        myActivity =this;
        ImageView mImageView = (ImageView) myActivity.findViewById(R.id.imageView);
        Picasso.with(this)
                .load("http://services.swpc.noaa.gov/images/animations/ovation-north/latest.jpg")
                .into(mImageView);

    }

}
