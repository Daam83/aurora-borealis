package com.daamapp.aurora;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


    }

    public void ovationNorth(View view) {

        Intent intent = new Intent(this, ovationSite.class);
        intent.putExtra("ovationSite", "north");


        startActivity(intent);


    }

    public void ovationSouth(View v) {

        Intent intent = new Intent(this, ovationSite.class);
        intent.putExtra("ovationSite", "south");


        startActivity(intent);


    }
}
