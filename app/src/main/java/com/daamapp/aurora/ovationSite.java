package com.daamapp.aurora;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

public class ovationSite extends AppCompatActivity {
    private Activity myActivity;
    private TextView textView;
    LoadHemisfere load = new LoadHemisfere();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ovation);
        Bundle bundle = getIntent().getExtras();


        myActivity = this;
        load.setInDate(bundle.getString("ovationSite"));
        reviceNewImage(load);

    }

    public void reviceNewImage(LoadHemisfere loadHemisfere) {
        ImageView mImageView = (ImageView) myActivity.findViewById(R.id.imageView);
        Picasso.with(this)
                .load(loadHemisfere.getToULR())

                .into(mImageView);
        textView = (TextView) findViewById(R.id.textView2);

        textView.setText(loadHemisfere.getToText());
    }

}
